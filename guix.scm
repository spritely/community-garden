;;; Commentary:
;;
;; Development environment for GNU Guix.
;;
;; To setup a development environment and build, run:
;;
;;    guix shell
;;    ./bootstrap
;;    ./configure
;;    make -j${nproc}
;;
;; To build the development snapshot, run:
;;
;;    guix build -f guix.scm
;;
;; To install the development snapshot, run:
;;
;;    guix install -f guix.scm
;;
;;; Code:
(use-modules (ice-9 match)
             (srfi srfi-1)
             (guix build-system gnu)
             (guix download)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (guix utils)
             (gnu packages)
             (gnu packages audio)
             (gnu packages autotools)
             (gnu packages fontutils)
             (gnu packages gl)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages image)
             (gnu packages maths)
             (gnu packages mp3)
             (gnu packages pkg-config)
             (gnu packages readline)
             (gnu packages sdl)
             (gnu packages texinfo)
             (gnu packages xiph))

(define target-guile guile-3.0-latest)

(define guile-opengl*
  (let ((commit "bd1ac49b43430d85a75130ea962210e687599a89"))
    (package
      (inherit guile-opengl)
      (version (string-append "0.2.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.savannah.gnu.org/git/guile-opengl.git")
                      (commit commit)))
                (sha256
                 (base32
                  "1iq21x8hxdq4gsbykiy1q9xy43psv7jjripw4raxdy93fvdasj50"))))
      (native-inputs (list autoconf automake pkg-config texinfo))
      (inputs (list freeglut glu mesa target-guile)))))

(define guile-sdl2
  (let ((commit "f7e5de964d82c036f029a458de14864eaca9e290"))
    (package
      (name "guile-sdl2")
      (version (string-append "0.8.0-1." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://git.dthompson.us/guile-sdl2.git")
                      (commit commit)))
                (sha256
                 (base32
                  "1dbdvm0551ii3hgpda127yyjl4v6iqz8rb3ccmi30nbyf0ry1lxr"))))
      (build-system gnu-build-system)
      (arguments
       '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
      (native-inputs (list autoconf automake pkg-config texinfo))
      (inputs (list target-guile sdl2))
      (synopsis "Guile bindings for SDL2")
      (description "Guile-sdl2 provides pure Guile Scheme bindings to the
SDL2 C shared library via the foreign function interface.")
      (home-page "https://git.dthompson.us/guile-sdl2.git")
      (license license:lgpl3+))))

(define chickadee
  (let ((commit "515ba05517c3fdf92aeb1e37c1ab5af320586604"))
    (package
     (name "chickadee")
     (version (string-append "0.8.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/chickadee.git")
                    (commit commit)))
              (sha256
               (base32
                "0xj46qclvmxj6djwpa7m1zx7ig2qppwppy294x767gkci2ansprz"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
     (native-inputs (list autoconf automake pkg-config texinfo))
     (inputs (list freetype
                   libjpeg-turbo
                   libpng
                   libvorbis
                   mpg123
                   openal
                   readline
                   target-guile))
     (propagated-inputs (list guile3.0-opengl guile-sdl2))
     (synopsis "Game development toolkit for Guile Scheme")
     (description "Chickadee is a game development toolkit for Guile
Scheme.  It contains all of the basic components needed to develop
2D/3D video games.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license license:gpl3+))))

(define catbird
  (let ((commit "bdb741ebb3a04adb1889a25c8f33c3ac9dff3b8c"))
    (package
     (name "catbird")
     (version (string-append "0.1.0-1." (string-take commit 7)))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.dthompson.us/catbird.git")
                    (commit commit)))
              (sha256
               (base32
                "17fghxj3yvc86y5b90zgsffacsgyv9gi3k92yl9gg4fm3v3gx1q4"))))
     (build-system gnu-build-system)
     (arguments
      '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
     (native-inputs (list autoconf automake pkg-config texinfo))
     (inputs (list target-guile))
     (propagated-inputs (list chickadee guile-sdl2))
     (synopsis "Game engine for Scheme programmers")
     (description "Catbird is a game engine written in Guile Scheme.")
     (home-page "https://dthompson.us/projects/chickadee.html")
     (license license:gpl3+))))

(define guile-goblins*
  (package
   (inherit guile-goblins)
   (name "guile-goblins")
   (version "0.10-git")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/spritely/guile-goblins.git")
                  (commit "2efcc90c26a929a6cbdb5bc34b0883f59f8e70f3")))
            (file-name (string-append name "-" version))
            (sha256
             (base32
              "0c0kprdbpzfg15v2bbw13w58kd24mnblp7444k5d6nxqkga5h3zj"))))
   (native-inputs (list autoconf automake pkg-config texinfo))))

(define %source-dir (dirname (current-filename)))

(package
 (name "community-garden")
 (version "0.1.0")
 (source (local-file %source-dir
                     #:recursive? #t
                     #:select? (git-predicate %source-dir)))
 (build-system gnu-build-system)
 (arguments
  '(#:make-flags '("GUILE_AUTO_COMPILE=0")))
 (native-inputs (list autoconf automake pkg-config texinfo))
 (inputs (list target-guile))
 (propagated-inputs (list catbird guile-goblins*))
 (synopsis "Spritely Goblins demo program")
 (description "Spritely Goblins demo program")
 (home-page "https://spritely.institute")
 (license license:asl2.0))
