(define-module (community-garden launch)
  #:use-module (catbird)
  #:use-module (catbird camera)
  #:use-module (catbird kernel)
  #:use-module (catbird node)
  #:use-module (catbird region)
  #:use-module (catbird scene)
  #:use-module (community-garden view)
  #:use-module (oop goops)
  #:export (launch-garden))

(define* (launch-garden node-thunk)
  (run-catbird
   (lambda ()
     (let ((region (create-full-region #:name 'main))
           (scene (make <scene> #:name 'garden)))
       (replace-scene region scene)
       (replace-major-mode scene (make <garden-mode>))
       (set! (camera region)
             (make <camera-2d>
               #:width %window-width
               #:height %window-height))
       (attach-to scene (node-thunk))))
   #:title "Community Garden"
   #:width %window-width
   #:height %window-height))
