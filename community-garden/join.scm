(define-module (community-garden join)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (community-garden actors)
  #:use-module (community-garden chickadee-vat)
  #:use-module (community-garden garden-bed)
  #:use-module (community-garden launch)
  #:use-module (community-garden plant)
  #:use-module (community-garden view)
  #:use-module (goblins)
  #:use-module (goblins ocapn ids)
  #:use-module (goblins ocapn captp)
  #:use-module (goblins ocapn netlayer onion)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:export (join-garden))

(define-class <garden-client> (<node-2d>)
  (vat #:accessor vat)
  (ocapn-vat #:getter ocapn-vat #:init-thunk spawn-vat)
  (user-name #:accessor user-name #:init-keyword #:user-name)
  (community-address #:accessor community-address
                     #:init-keyword #:community-address)
  (community #:accessor community)
  (user #:accessor user))

(define-method (initialize (client <garden-client>) initargs)
  (define (boot-garden)
    (define community-sref (string->ocapn-id (community-address client)))
    (define onion-netlayer (new-onion-netlayer))
    (define mycapn (spawn-mycapn onion-netlayer))
    (run-script client
      (with-vat (vat client)
        (set! (community client)
              (<- mycapn 'enliven community-sref))
        (set! (user client)
              (<- (community client) 'register-gardener (user-name client)))
        (attach-to client
                   (make <garden-view>
                     #:name 'garden
                     #:vat (vat client)
                     #:user (user client))))))
  (next-method)
  (resize client %window-width %window-height)
  (set! (vat client) (make-chickadee-vat #:agenda (agenda client)))
  (vat-start! (vat client))
  (call-with-vat (ocapn-vat client) boot-garden))

(define (join-garden user-name community-address)
  (launch-garden
   (lambda ()
     (make <garden-client>
       #:name 'root
       #:user-name user-name
       #:community-address community-address))))
