(define-module (community-garden host)
  #:use-module (catbird node)
  #:use-module (catbird node-2d)
  #:use-module (community-garden actors)
  #:use-module (community-garden chickadee-vat)
  #:use-module (community-garden garden-bed)
  #:use-module (community-garden launch)
  #:use-module (community-garden plant)
  #:use-module (community-garden view)
  #:use-module (goblins)
  #:use-module (goblins ocapn ids)
  #:use-module (goblins ocapn captp)
  #:use-module (goblins ocapn netlayer onion)
  #:use-module (oop goops)
  #:export (host-garden))

(define-class <garden-host> (<node-2d>)
  (vat #:accessor vat)
  (ocapn-vat #:getter ocapn-vat #:init-thunk spawn-vat)
  (garden-name #:accessor garden-name #:init-keyword #:garden-name)
  (user-name #:accessor user-name #:init-keyword #:user-name)
  (garden #:accessor garden)
  (community #:accessor community)
  (user #:accessor user))

(define-method (initialize (host <garden-host>) initargs)
  (next-method)
  (resize host %window-width %window-height)
  (set! (vat host) (make-chickadee-vat #:agenda (agenda host)))
  (vat-start! (vat host))
  (run-script host
    (define (boot-garden)
      (define the-botanist (spawn ^botanist))
      (define the-garden-gate (spawn ^garden-gate the-botanist))
      (define sunflower/approved
        ($ the-botanist 'approve-plant sunflower))
      (define cabbage/approved
        ($ the-botanist 'approve-plant cabbage))
      (set! (garden host)
            (spawn ^garden
                   (garden-name host)
                   (make-garden-bed 8 8)
                   the-garden-gate))
      (set! (community host)
            (spawn ^garden-community (garden host)))
      (set! (user host)
            ($ (community host) 'register-gardener (user-name host)))
      (attach-to host
                 (make <garden-view>
                   #:name 'garden
                   #:vat (vat host)
                   #:user (user host)))
      (with-vat (ocapn-vat host)
        (define onion-netlayer (new-onion-netlayer))
        (define mycapn (spawn-mycapn onion-netlayer))
        (let ((community-sref ($ mycapn 'register (community host) 'onion)))
          (format #t "Connect to: ~a\n" (ocapn-id->string community-sref)))))
    (call-with-vat (vat host) boot-garden)))

(define (host-garden garden-name user-name)
  (launch-garden
   (lambda ()
     (make <garden-host>
       #:name 'root
       #:garden-name garden-name
       #:user-name user-name))))
